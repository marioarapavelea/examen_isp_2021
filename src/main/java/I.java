interface U {

}

public class I implements U {
    private long t;
    private K k;

    public I() {

    }

    public void f() {
        System.out.println("f");
    }

    public void i(J j) {
        System.out.println(j.getJ());
    }

}

class J {
    private long j;

    public J() {

    }

    public long getJ() {
        return j;
    }

    public void setJ(long j) {
        this.j = j;
    }
}

class K {
    private long k;

    public K() {

    }

    public K(long k) {
        this.k = k;
    }

    public long getK() {
        return k;
    }

    public void setK(long k) {
        this.k = k;
    }

}

class N {
    private long n;
    private I i;

    public N() {

    }

    public long getN() {
        return n;
    }

    public void setN(long n) {
        this.n = n;
    }
}

class L {
    private K k;

    public long metA() {
        return k.getK();
    }

    public L() {
        this.k = new K();
        k.setK(5);
    }
}

class S {
    K k;

    public void metB() {
        long rK = k.getK();
    }

    public S() {

    }

}

