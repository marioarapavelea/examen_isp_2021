import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;
import java.awt.*;

public class S2 implements ActionListener {
    JFrame frame = new JFrame();
    JButton button = new JButton("Press");
    JTextArea textArea = new JTextArea();
    JPanel panel = new JPanel();
    JTextField textField1 = new JTextField();
    JTextField textField2 = new JTextField();

    public S2() {
        panel.setBorder(BorderFactory.createEmptyBorder(30, 60, 10, 30));
        panel.setLayout(new GridLayout(0, 1));
        panel.add(button);
        panel.add(textField1);
        panel.add(textField2);
        panel.add(textArea);

        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Sub2_Examen2020");
        frame.pack();
        frame.setVisible(true);

        button.addActionListener(this);
    }


    public static void main(String[] args) {
        new S2();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        double value1 = Double.parseDouble(textField1.getText());
        double value2 = Double.parseDouble(textField2.getText());
        double value = value1 + value2;
        textArea.setText("The sum is: " + value);
    }
}
